apiVersion: apps/v1
kind: Deployment
metadata:
  {{- if .Values.name }}
  name: {{ printf "%s-%s" (include "agepoly-base-chart.fullname" .) .Values.name }}
  {{- else }}
  name: {{ include "agepoly-base-chart.fullname" . }}
  {{- end }}
  labels:
    {{- include "agepoly-base-chart.labels" . | nindent 4 }}
spec:
  {{- if not .Values.autoscaling.enabled }}
  replicas: {{ .Values.replicaCount | default 1 }}
  {{- end }}
  {{- if .Values.rollingUpdateEnabled }}
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxSurge: 2
      maxUnavailable: 0
  {{- end }}
  selector:
    matchLabels:
      {{- include "agepoly-base-chart.selectorLabels" . | nindent 6 }}
  template:
    metadata:
      {{- with .Values.podAnnotations }}
      annotations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      labels:
        {{- include "agepoly-base-chart.selectorLabels" . | nindent 8 }}
        {{- if .Values.global.production }}
        {{- /* this allow to force restart the pods when the secrets are changed */}}
        {{- $maybesecret := (lookup "v1" "Secret" .Values.global.namespace (print $.Release.Name "-prod-secret")) -}}
        {{- if and $maybesecret $maybesecret.metadata }}
        checksum/secret: {{ $maybesecret.metadata.resourceVersion | sha256sum | trunc 63 }}
        {{- end }}
        {{- end }}
    spec:
      {{- with .Values.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      serviceAccountName: {{ include "agepoly-base-chart.serviceAccountName" . }}
      securityContext:
        {{- toYaml .Values.podSecurityContext | nindent 8 }}
      containers:
        - name: {{ .Chart.Name }}
          securityContext:
            {{- toYaml .Values.securityContext | nindent 12 }}
          image: "{{ .Values.image.repository }}:{{ .Values.image.tag | default .Values.image.default_tag }}"
          {{- if .Values.command }}
          command: {{ .Values.command }}
          {{- end }}
          {{- if .Values.args }}
          args: {{ .Values.args }}
          {{- end }}
          imagePullPolicy: {{ .Values.image.pullPolicy }}
          ports:
            {{- toYaml .Values.ports | nindent 12 }}
          {{- if .Values.livenessProbe }}
          livenessProbe:
            {{- toYaml .Values.livenessProbe | nindent 12 }}
          {{- end }}
          {{- if .Values.readinessProbe }}
          readinessProbe:
            {{- toYaml .Values.readinessProbe | nindent 12 }}
          {{- end }}
          resources:
            {{- if and (not .Values.global.production) .Values.testResources }}
            {{- toYaml .Values.testResources | nindent 12 }}
            {{- else }}
            {{- toYaml .Values.resources | nindent 12 }}
            {{- end}}
          env:
            {{- if and .Values.needDatabase .Values.global.production }}
            # postgres doesnt want - in db names and kubernetes doesnt want _ in secret name
            - name: "DB_HOST"
              value: "agepoly-production.postgres-operator.svc.cluster.local"
            - name: "DB_PORT"
              value: "5432"
            - name: "DB_NAME"
              value: "{{ .Values.global.namespace | replace "-" "_" | replace "." "_"  }}_{{ .Values.global.name | replace "-" "_"  | replace "." "_"  }}"
            - name: "DB_USER"
              valueFrom:
                secretKeyRef:
                  name: "{{ .Values.global.namespace }}.{{ .Values.global.namespace }}-{{ .Values.global.name }}.agepoly-production.credentials.postgresql.acid.zalan.do"
                  key: "username"    
            - name: "DB_PASSWORD"             
              valueFrom:
                secretKeyRef:
                  name: "{{ .Values.global.namespace }}.{{ .Values.global.namespace }}-{{ .Values.global.name }}.agepoly-production.credentials.postgresql.acid.zalan.do"
                  key: "password"         
            {{- end }}
            {{- if and .Values.needDatabase .Values.global.staging }}
            - name: "DB_HOST"
              value: "database.database.svc.cluster.local"
            - name: "DB_PORT"
              value: "5432"
            - name: "DB_NAME"
              value: "{{ .Values.global.namespace }}-{{ .Values.global.name }}-staging"
            - name: "DB_USER"
              value: "{{ .Values.global.name }}-staging"
            - name: "DB_PASSWORD"             
              valueFrom:
                secretKeyRef:
                  name: "{{ .Values.global.name }}-staging.{{ .Values.global.namespace }}-{{ .Values.global.name }}-agepoly-staging.credentials.postgresql.acid.zalan.do"
                  key: "password"
            {{- end }}
            {{- if and .Values.needDatabase (and (not .Values.global.production) (not .Values.global.staging)) }}
            - name: "DB_HOST"
              value: "{{ include "agepoly-base-chart.name" . }}-{{ .Values.global.branch  }}-test-db.{{ .Values.global.namespace }}.svc.cluster.local"
            - name: "DB_PORT"
              value: "5432"
            - name: "DB_NAME"
              value: "test"
            - name: "DB_USER"
              value: "test"
            - name: "DB_PASSWORD"             
              value: "test"
            {{- end }}
            {{- range $env := .Values.envFromSecrets }}
            - name: {{ $env.name | quote }}
              {{- if not $.Values.global.production }}
              value: {{ $env.test_value | quote }}
              {{- else }}
              valueFrom:
                secretKeyRef:
                  {{- if $env.secretName }}
                  name: {{ $env.secretName | quote }}
                  {{- else }}
                  name: "{{ $.Release.Name }}-prod-secret"
                  {{- end }}
                  {{- if $env.secretKey }}
                  key: {{ $env.secretKey | quote }}
                  {{- else}}
                  key: {{ $env.name | quote }}
                  {{- end}}
              {{- end }}
            {{- end }}            
            {{- range $env := .Values.env }}
            - name: {{ $env.name | quote }}
              value: {{ $env.value | quote }}
            {{- end }}
          volumeMounts:
          {{- range $vol := .Values.persistentVolumes }}
          - name: {{ $vol.name | quote }}
            mountPath: {{ $vol.path | quote }}
          {{- end }} 
      volumes:
      {{- range $vol := .Values.persistentVolumes }}
        - name: {{ $vol.name | quote }}
          persistentVolumeClaim:
            claimName: {{ include "agepoly-base-chart.fullname" $ }}-{{ $vol.name }}
      {{- end }}                  
      {{- with .Values.nodeSelector }}
      nodeSelector:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.affinity }}
      affinity:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- with .Values.tolerations }}
      tolerations:
        {{- toYaml . | nindent 8 }}
      {{- end }}
---
{{- if and .Values.needDatabase (and (not .Values.global.production) (not .Values.global.staging)) }}
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ include "agepoly-base-chart.name" . }}-{{ .Values.global.branch  }}-test-db
spec:
  replicas: 1
  selector:
    matchLabels:
      app.kubernetes.io/name: {{ include "agepoly-base-chart.name" . }}-{{ .Values.global.branch  }}-test-db
  template:
    metadata:
      labels:
        app: postgres
        branch: "{{ .Values.global.branch  }}"
        helm.sh/chart: {{ include "agepoly-base-chart.chart" . }}
        app.kubernetes.io/managed-by: {{ .Release.Service }}
        app.kubernetes.io/name: {{ include "agepoly-base-chart.name" . }}-{{ .Values.global.branch  }}-test-db
        app.kubernetes.io/component: postgres-test-db
    spec:
      containers:
        - name: postgres
          image: postgres:15.1
          imagePullPolicy: "IfNotPresent"
          ports:
            - containerPort: 5432
          resources:
            limits:
              cpu: 100m
              memory: 80Mi
            requests:
              cpu: 100m
              memory: 80Mi
          env:
            - name: "POSTGRES_PASSWORD"
              value: "test"
            - name: "POSTGRES_USER"
              value: "test"
{{- end }}
