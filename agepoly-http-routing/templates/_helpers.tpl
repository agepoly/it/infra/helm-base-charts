{{/*
Expand the name of the chart.
*/}}
{{- define "agepoly-http-routing.name" -}}
{{- print .Values.global.name | trunc 63 | trimSuffix "-" }}
{{- end }}


{{/*
Create a default fully qualified app name.
We truncate at 63 chars because some Kubernetes name fields are limited to this (by the DNS naming spec).
If release name contains chart name it will be used as a full name.
*/}}
{{- define "agepoly-http-routing.fullname" -}}
{{- if contains .Values.global.name .Release.Name }}
{{- .Release.Name | trunc 63 | trimSuffix "-" }}
{{- else }}
{{- printf "%s-%s" .Release.Name .Values.global.name | trunc 63 | trimSuffix "-" }}
{{- end }}
{{- end }}


{{/*
Create chart name and version as used by the chart label.
*/}}
{{- define "agepoly-http-routing.chart" -}}
{{- printf "%s-%s" .Chart.Name .Chart.Version | replace "+" "_" | trunc 63 | trimSuffix "-" }}
{{- end }}


{{/*
Common labels
*/}}
{{- define "agepoly-http-routing.labels" -}}
helm.sh/chart: {{ include "agepoly-http-routing.chart" . }}
{{ include "agepoly-http-routing.selectorLabels" . }}
{{- if .Values.AppVersion }}
app.kubernetes.io/version: {{ .Values.AppVersion | quote }}
{{- end }}
app.kubernetes.io/managed-by: {{ .Release.Service }}
{{- end }}

{{/*
Selector labels
*/}}
{{- define "agepoly-http-routing.selectorLabels" -}}
app.kubernetes.io/name: {{ include "agepoly-http-routing.name" . }}
app.kubernetes.io/instance: {{ .Release.Name }}
{{- end }}
